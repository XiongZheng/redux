import React from 'react'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getUserDetails, clickLike} from "../actions/userActions";

class UserProfile extends React.Component {
  componentDidMount() {
    this.props.getUserDetails(this.props.match.params.id);

  }

  // handlerClickLike() {
  //   this.props.clickLike(this.props.userDetails.name, this.props.match.params.id);
  //   this.forceUpdate();
  // }

  render() {
    const user = this.props.userDetails;
    // const   linked = this.props.linkedIds.has(this.props.match.params.id);

    return (
      <div>
        <p>
          UserName:{user.name}
        </p>
        <p>
          Gender:{user.gender}
        </p>
        <p>
          Description:{user.description}
        </p>
        {/*{*/}
        {/*  linked ?*/}
        {/*    <button onClick={this.handlerClickLike.bind(this)} className="liked">LIKED</button> :*/}
        {/*    <button onClick={this.handlerClickLike.bind(this)} className="like">LIKE</button>*/}
        {/*}*/}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userDetails: state.user.userDetails
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getUserDetails,
  clickLike}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
