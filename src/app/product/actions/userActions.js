export const getUserDetails = (id) => (dispatch) => {
  fetch('http://localhost:8080/api/user-profiles/'+id)
    .then(response => response.json())
    .then(result=>{
      dispatch ({
        type: 'GET_USER_DETAILS',
        userDetails: result
      });
    })
};

export const clickLike = (uname, id) => (dispatch) => {
  const data = { userProfileId: id, userName: uname };
  fetch('http://localhost:8080/api/like', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    dispatch({
      type: 'LINKED_USER',
      linkedId: id
    });
  })
};

