const initState = {
  userDetails: {},
  linkedId: new Set()
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_USER_DETAILS':
      return {
        ...state,
        userDetails: action.userDetails
      };

    case "LINKED_USER":return {
      ...state,
      linkedIds: state.linkedIds.add(action.linkedId)
    };
    default:
      return state
  }
};
