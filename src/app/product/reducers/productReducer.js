const initState = {
  productDetails: {}
};

export default (state = initState, action) => {
  if (action.type === 'GET_PRODUCT_DETAILS') {
    return {
      ...state,
      productDetails: action.productDetails
    };
  } else {
    return state
  }
};
